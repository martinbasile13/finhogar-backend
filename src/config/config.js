require('dotenv').config()

module.exports = {
    app: {
        port: process.env.PORT || 3000,
    },
    mysql2: {
        host: process.env.AZURE_HOST || "finhogar-database.mysql.database.azure.com",
        database: process.env.AZURE_DATABASES || 'finhogar',
        user: process.env.AZURE_USER || 'martin',
        password: process.env.AZURE_PASSWORD || 'Basile13',
        azure_cert: __dirname + "/DigiCertGlobalRootCA.crt.pem"
    }
}