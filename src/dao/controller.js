const db = require('../db/mysql.js')


const TABLA= 'ingresos';

function list(){
    return db.list(TABLA);
}
function retrieve(id){
    return db.retrieve(TABLA, id)
}
function create(body){
    return db.create(TABLA, body)
}
function update(body){
    return db.update(TABLA, body)
}
function destroy(id){
    return db.destroy(TABLA, id)
}


module.exports = {
    list,
    retrieve,
    create,
    update,
    destroy
}