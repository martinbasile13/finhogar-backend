const express = require('express')
const router = express.Router();

const rest = require('../red/rest.js')
const controller = require('./controller.js')

//list
router.get('/', async (req, res) => {
  try{
    const items = await controller.list()
    rest.sucess(req, res, items, 200)
  }catch(err){
    rest.error(req, res, err, 500)
  }
})
//retrative
router.get('/:id', async (req, res) => {
  try{
    const items = await controller.retrieve(req.params.id)
    rest.sucess(req, res, items, 200)
  }catch(err){
    rest.error(req, res, err, 500)
  }
})
//create
router.post('/', async (req, res) => {
  try{
    const items = await controller.create([req.body.nombre, req.body.tipo, req.body.procedencia, req.body.frecuencia, req.body.monto])
    rest.sucess(req, res, items, 200)
  }catch(err){
    rest.error(req, res, err, 500)
  }
})
//update
router.put('/:id', async (req, res) => {
  try{
    const items = await controller.update([req.body.nombre, req.body.tipo, req.body.procedencia, req.body.frecuencia, req.body.monto, req.params.id])
    rest.sucess(req, res, items, 200)
  }catch(err){
    rest.error(req, res, err, 500)
  }
})

//delete
router.delete('/:id', async (req, res) => {
  try{
    const items = await controller.destroy(req.params.id)
    rest.sucess(req, res, items, 200)
  }catch(err){
    rest.error(req, res, err, 500)
  }
})

module.exports = router;