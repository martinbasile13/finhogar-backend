const mysql2 = require('mysql2')
const config = require('../config/config.js')
const fs = require('fs')

const dbConfig = {
    host: config.mysql2.host,
    database: config.mysql2.database,
    user: config.mysql2.user,
    password: config.mysql2.password,
    ssl:{
        ca:fs.readFileSync(config.mysql2.azure_cert)
    },
}
 
let conexion;

function conMysql(){
    conexion = mysql2.createConnection(dbConfig)

    conexion.connect((err)=> {
        if(err){
            console.log('[error en conexion de base de datos]', err)
            setTimeout(conMysql, 200)
        }else{
            console.log('[Base de datos conectada en la nube `Azure`]')
        }
    });
}

conMysql()


function list(tabla) {
    return new Promise((req, res) => {
        conexion.query(`SELECT * FROM ${tabla}`, (err, result) => {
            if(err) return res(err)
            req(result);
        })
    })
}

function retrieve(tabla, id) {
    return new Promise((req, res) => {
        conexion.query(`SELECT * FROM ${tabla} WHERE id=${id}`, (err, result) => {
            if(err) return res(err)
            req(result);
        })
    })
}

function update(tabla, body) {
    return new Promise((req, res) => {
        conexion.query(`UPDATE ${tabla} SET nombre = ?, tipo = ?, procedencia = ?, frecuencia = ? , monto = ? WHERE id = ?`,body, (err, result) => {
            if(err) return res(err)
            req(result);
        })
    })
}
function create(tabla, body) {
    return new Promise((req, res) => {
        conexion.query(`INSERT INTO ${tabla} (nombre, tipo, procedencia, frecuencia, monto) VALUES (?, ?, ?, ?, ?)`,body, (err, result) => {
            if(err) return res(err)
            req(result);
        })
    })
}

function destroy(tabla, id) {
    return new Promise((req, res) => {
        conexion.query(`DELETE FROM ${tabla} WHERE id= ?`,id, (err, result) => {
            if(err) return res(err)
            req(result);
        })
    })
}

module.exports = {
    list,
    retrieve,
    update,
    create,
    destroy
}